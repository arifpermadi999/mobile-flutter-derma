import 'package:derma_travel/common/alert_helper.dart';
import 'package:derma_travel/common/color_const.dart';
import 'package:derma_travel/data/bloc/login/login_bloc.dart';
import 'package:derma_travel/presentation/main_menu.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

class LoginScreen extends StatefulWidget {
  static String route = "login";

  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  late Size size;
  bool isPassword = true;
  @override
  void initState() {
    super.initState();

    context.read<LoginBloc>().add(CekSessionLoginEvent());
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        padding: const EdgeInsets.only(top: 100),
        decoration: BoxDecoration(color: ColorConst().secondaryColor),
        child: Column(
          children: <Widget>[
            Image.asset(
              'assets/images/logo.png',
              width: 100.0,
              height: 100.0,
            ),
            Container(
              alignment: Alignment.centerLeft,
              padding: const EdgeInsets.only(left: 40, top: 20),
              child: const Text(
                "Sign In",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 36),
                textAlign: TextAlign.left,
              ),
            ),
            SizedBox(height: size.height * 0.03),
            Container(
              alignment: Alignment.centerLeft,
              padding: const EdgeInsets.symmetric(horizontal: 40),
              child: const Text(
                "Email",
                textAlign: TextAlign.left,
              ),
            ),
            Container(
              alignment: Alignment.center,
              margin: const EdgeInsets.symmetric(horizontal: 40),
              child: TextFormField(
                controller: emailController,
                decoration: const InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  contentPadding: EdgeInsets.only(left: 10, right: 10),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: size.height * 0.03),
            Container(
              alignment: Alignment.centerLeft,
              padding: const EdgeInsets.symmetric(horizontal: 40),
              child: const Text(
                "Password",
                textAlign: TextAlign.left,
              ),
            ),
            Container(
              alignment: Alignment.center,
              margin: const EdgeInsets.symmetric(horizontal: 40),
              child: BlocBuilder<LoginBloc, LoginState>(
                builder: (context, state) {
                  if (state is VisiblePasswordState) {
                    isPassword = state.isPassword;
                  }
                  return TextFormField(
                    controller: passwordController,
                    obscureText: isPassword,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      suffixIcon: IconButton(
                        icon: Icon(
                          isPassword == true
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color: Theme.of(context).primaryColorDark,
                        ),
                        onPressed: () {
                          isPassword = !isPassword;

                          context
                              .read<LoginBloc>()
                              .add(VisiblePasswordEvent(isPassword));
                        },
                      ),
                      contentPadding:
                          const EdgeInsets.only(left: 10, right: 10),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.black,
                        ),
                      ),
                      enabledBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.black,
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
            SizedBox(height: size.height * 0.05),
            BlocConsumer<LoginBloc, LoginState>(
              listener: (context, state) {
                if (state is SuccessLoginState) {
                  context
                      .push("/${MainMenu.route}")
                      .then((value) => SystemNavigator.pop());
                  //Navigator.pop(context);
                }
                if (state is ErrorMessageState) {
                  AlertHelper().errorAlert(context, state.mesage);
                }
                if (state is HasLoginState) {
                  context
                      .push("/${MainMenu.route}")
                      .then((value) => SystemNavigator.pop());
                  //Navigator.pop(context);
                }
              },
              builder: (context, state) {
                if (state is LoadingState) {
                  return const Center(child: CircularProgressIndicator());
                }
                return Container(
                  alignment: Alignment.centerRight,
                  margin: const EdgeInsets.symmetric(horizontal: 40),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.zero),
                        backgroundColor: ColorConst().primaryColor),
                    onPressed: () async {
                      context.read<LoginBloc>().add(LoginActionEvent(
                          emailController.text, passwordController.text));
                    },
                    child: Container(
                      alignment: Alignment.center,
                      height: 50.0,
                      width: size.width,
                      padding: const EdgeInsets.all(0),
                      child: const Text(
                        "SIGN IN",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white),
                      ),
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
