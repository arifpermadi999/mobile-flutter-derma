import 'package:derma_travel/common/color_const.dart';
import 'package:derma_travel/presentation/tab/filter_tab.dart';
import 'package:derma_travel/presentation/tab/saved_tab.dart';
import 'package:derma_travel/presentation/tab/search_tab.dart';
import 'package:flutter/material.dart';

class MainMenu extends StatefulWidget {
  static String route = "main-menu";
  const MainMenu({super.key});

  @override
  State<MainMenu> createState() => _SearchTabState();
}

class _SearchTabState extends State<MainMenu>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: TabBarView(controller: _tabController, children: [
        SearchTab(),
        const SavedTab(),
        const FilterTab(),
      ]),
      bottomNavigationBar: TabBar(
        controller: _tabController,
        indicatorColor: ColorConst().primaryColor,
        labelColor: ColorConst().primaryColor,
        unselectedLabelColor: Colors.grey,
        tabs: const [
          Tab(
            icon: Icon(Icons.search),
            text: "Search",
          ),
          Tab(
            icon: Icon(Icons.favorite),
            text: "Saved",
          ),
          Tab(
            icon: Icon(Icons.filter),
            text: "Filter",
          ),
        ],
      ),
    );
  }
}
