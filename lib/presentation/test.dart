import 'package:derma_travel/common/map_utils.dart';
import 'package:flutter/material.dart';
import 'package:whatsapp_share/whatsapp_share.dart';

class Test extends StatelessWidget {
  const Test({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: () =>
                MapUtils.openMap(-6.251415534911771, 107.10216887143712),
            child: const Padding(
              padding: EdgeInsets.only(left: 40, bottom: 30),
              child: Row(
                children: [Icon(Icons.directions), Text("Get Directions")],
              ),
            ),
          ),
          GestureDetector(
            onTap: () async {
              await WhatsappShare.share(
                text: 'Whatsapp share text',
                linkUrl: 'https://flutter.dev/',
                phone: '911234567890',
              );
            },
            child: const Padding(
              padding: EdgeInsets.only(left: 50),
              child: Row(
                children: [Icon(Icons.share), Text("Share")],
              ),
            ),
          )
        ],
      ),
    );
  }
}
