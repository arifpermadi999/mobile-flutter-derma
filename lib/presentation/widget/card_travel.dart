import 'package:derma_travel/data/models/tourism_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CardTourism extends StatelessWidget {
  final Function onDoubleClick;
  final Function onFavClick;
  final Function onClick;
  final int isFavorite;
  final Tourism tourism;
  final bool isLoading;
  const CardTourism(
      {super.key,
      required this.tourism,
      required this.onDoubleClick,
      required this.onFavClick,
      required this.onClick,
      required this.isFavorite,
      this.isLoading = false});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onClick(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
            onDoubleTap: () => onDoubleClick(),
            child: Stack(
              children: [
                Image.network(tourism.image ?? ""),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: GestureDetector(
                      onTap: () {
                        onFavClick();
                      },
                      child: Icon(
                        Icons.favorite,
                        color: isFavorite == 1 ? Colors.red : Colors.grey,
                        size: 30,
                      )),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Row(
              children: [
                Image.asset(
                    height: 30.0, width: 30.0, 'assets/images/anchor.png'),
                const Padding(padding: EdgeInsets.only(right: 10)),
                Text(
                  tourism.name ?? "",
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 16),
                )
              ],
            ),
          ),
          Padding(
              padding: const EdgeInsets.only(bottom: 10, top: 10),
              child: Text(tourism.description != null
                  ? "${tourism.description!.substring(0, 100)}..."
                  : ""))
        ],
      ),
    );
  }
}
