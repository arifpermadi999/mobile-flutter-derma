import 'package:derma_travel/common/alert_helper.dart';
import 'package:derma_travel/common/color_const.dart';
import 'package:derma_travel/common/map_utils.dart';
import 'package:derma_travel/data/bloc/detail_tourism/detail_tourism_bloc.dart';
import 'package:derma_travel/data/models/tourism_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatsapp_share/whatsapp_share.dart';

class DetailTourism extends StatelessWidget {
  static String route = "detail-tourism";
  Tourism tourism;
  DetailTourism({super.key, required this.tourism});
  double lattitude = 0;
  double longitude = 0;
  @override
  Widget build(BuildContext context) {
    if (tourism.latlong != null) {
      var latlng = tourism.latlong?.split(",");
      lattitude = double.parse(latlng![0]);
      longitude = double.parse(latlng[1]);
    }
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(color: ColorConst().secondaryColor),
        padding: const EdgeInsets.only(left: 23, right: 23, top: 20),
        child: Column(
          children: [
            Expanded(
              flex: 6,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Align(
                      alignment: Alignment.centerRight,
                      child: GestureDetector(
                        onTap: () => Navigator.pop(context),
                        child: const Icon(
                          Icons.close,
                          color: Colors.black,
                          size: 28,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 40),
                      child: Text(
                        tourism.name ?? "",
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 24),
                      ),
                    ),
                    Stack(
                      children: [
                        Image.network(
                          tourism.image ?? "",
                          height: 400,
                          fit: BoxFit.cover,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: GestureDetector(
                              onTap: () {
                                if (tourism.isFavorite == 0) {
                                  tourism.isFavorite = 1;
                                } else {
                                  tourism.isFavorite = 0;
                                }
                                context.read<DetailTourismBloc>().add(
                                    SetFavoriteEvent(
                                        isFavorite: tourism.isFavorite ?? 0,
                                        id: tourism.id ?? 0));
                              },
                              child: BlocConsumer<DetailTourismBloc,
                                  DetailTourismState>(
                                listener: (context, state) {
                                  if (state is SuccessMessageState) {
                                    AlertHelper()
                                        .successAlert(context, state.message);
                                  }
                                  if (state is ErrorMessageState) {
                                    AlertHelper()
                                        .errorAlert(context, state.message);
                                  }
                                },
                                builder: (context, state) {
                                  if (state is LoadingState) {
                                    return const CircularProgressIndicator();
                                  }
                                  if (state is SetFavoriteState) {
                                    tourism.isFavorite = state.isFavorite;
                                  }
                                  return Icon(
                                    Icons.favorite,
                                    color: tourism.isFavorite == 1
                                        ? Colors.red
                                        : Colors.grey,
                                    size: 30,
                                  );
                                },
                              )),
                        ),
                      ],
                    ),
                    Padding(
                        padding:
                            const EdgeInsets.only(top: 10, left: 10, right: 10),
                        child: Text(tourism.description ?? ""))
                  ],
                ),
              ),
            ),
            Expanded(
                flex: 1,
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: GestureDetector(
                        onTap: () => MapUtils.openMap(lattitude, longitude),
                        child: const Padding(
                          padding: EdgeInsets.only(left: 40),
                          child: Row(
                            children: [
                              Icon(Icons.directions),
                              Text("Get Directions")
                            ],
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: GestureDetector(
                        onTap: () async {
                          await WhatsappShare.share(
                            text: tourism.invitationText ?? "",
                            linkUrl: 'https://flutter.dev/',
                            phone: '911234567890',
                          );
                        },
                        child: const Padding(
                          padding: EdgeInsets.only(left: 50),
                          child: Row(
                            children: [Icon(Icons.share), Text("Share")],
                          ),
                        ),
                      ),
                    ),
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
