import 'package:derma_travel/common/color_const.dart';
import 'package:derma_travel/data/bloc/search_tab/search_tab_bloc.dart';
import 'package:derma_travel/data/models/auth_model.dart';
import 'package:derma_travel/data/models/tourism_model.dart';
import 'package:derma_travel/presentation/tab/detail_tourism.dart';
import 'package:derma_travel/presentation/widget/card_travel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

class SearchTab extends StatelessWidget {
  SearchTab({super.key});
  String search = "";
  List<Tourism> data = [];
  bool onSearch = false;
  bool onLoading = false;
  TextEditingController searchEditingController = TextEditingController();
  int isFavorite = 0;
  AuthModel authModel = AuthModel();

  @override
  Widget build(BuildContext context) {
    context.read<SearchTabBloc>().add(CekLoginSessionEvent());
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(color: ColorConst().secondaryColor),
        padding: const EdgeInsets.only(top: 80, left: 20, right: 20),
        child: BlocConsumer<SearchTabBloc, SearchTabState>(
          listener: (context, state) {},
          builder: (context, state) {
            if (state is GetSessionLoginState) {
              authModel = state.authModel;
            }
            if (state is LoadingState) {}

            if (state is SearchCloseState) {
              data = [];
              searchEditingController.text = "";
            }
            if (state is LoadDataState) {
              data = state.data;
            }
            if (state is SetFavoriteState) {
              data[state.index].isFavorite = state.isFavorite;
            }
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                onSearch
                    ? const SizedBox()
                    : Text(
                        "Hi ${authModel.user?.name},\nwhere would you like to go?",
                        style: const TextStyle(
                            fontSize: 33, fontWeight: FontWeight.bold),
                      ),
                Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 10),
                  child: TextField(
                    textInputAction: TextInputAction.go,
                    controller: searchEditingController,
                    onSubmitted: (value) {
                      onSearch = true;
                      context
                          .read<SearchTabBloc>()
                          .add(SearchEvent(searchEditingController.text));
                    },
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.all(0),
                      prefixIcon: GestureDetector(
                          onTap: () {
                            onSearch = true;
                            context
                                .read<SearchTabBloc>()
                                .add(SearchEvent(searchEditingController.text));
                          },
                          child: onSearch == false
                              ? const SizedBox()
                              : const Icon(Icons.search)),
                      suffixIcon: GestureDetector(
                          onTap: () {
                            context
                                .read<SearchTabBloc>()
                                .add(SearchCloseEvent());
                          },
                          child: onSearch == false
                              ? const SizedBox()
                              : const Icon(Icons.close)),
                      labelText: "Search ",
                      enabledBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.grey,
                        ),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: Colors.blue),
                      ),
                    ),
                  ),
                ),
                data.isEmpty
                    ? const SizedBox()
                    : Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Text(
                          "${data.length} results found",
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                data.isEmpty
                    ? const SizedBox()
                    : Expanded(
                        child: ListView.builder(
                            itemCount: data.length,
                            itemBuilder: (context, index) {
                              return CardTourism(
                                tourism: data[index],
                                isFavorite: data[index].isFavorite ?? 0,
                                onClick: () {
                                  context
                                      .push("/${DetailTourism.route}",
                                          extra: data[index])
                                      .then((value) => context
                                          .read<SearchTabBloc>()
                                          .add(SearchEvent(
                                              searchEditingController.text)));
                                },
                                onDoubleClick: () {
                                  if (data[index].isFavorite == 0) {
                                    isFavorite = 1;
                                  } else {
                                    isFavorite = 0;
                                  }
                                  context.read<SearchTabBloc>().add(
                                      SetFavoriteEvent(
                                          isFavorite: isFavorite,
                                          id: data[index].id ?? 0,
                                          index: index));
                                },
                                onFavClick: () {
                                  if (data[index].isFavorite == 0) {
                                    isFavorite = 1;
                                  } else {
                                    isFavorite = 0;
                                  }
                                  context.read<SearchTabBloc>().add(
                                      SetFavoriteEvent(
                                          isFavorite: isFavorite,
                                          id: data[index].id ?? 0,
                                          index: index));
                                },
                              );
                            }),
                      )
              ],
            );
          },
        ),
      ),
    );
  }
}
