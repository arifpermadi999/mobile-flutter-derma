import 'package:flutter/material.dart';

class ColorConst {
  Color primaryColor = const Color(0xFFEC2761);
  Color secondaryColor = const Color(0xFFE1F5FF);
}
