import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class AlertHelper {
  void deleteAlert(BuildContext context, Function deleteAction) {
    Alert(
      context: context,
      type: AlertType.warning,
      title: "Information",
      desc: "Are you sure want to delete this data?",
      buttons: [
        DialogButton(
          onPressed: () => deleteAction(),
          color: Colors.red,
          child: const Text(
            "Yes",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        ),
        DialogButton(
          onPressed: () => Navigator.pop(context),
          color: Colors.grey,
          child: const Text(
            "No",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        )
      ],
    ).show();
  }

  void errorAlert(BuildContext context, String message) {
    Alert(
      context: context,
      type: AlertType.error,
      title: "Information",
      desc: message,
      buttons: [
        DialogButton(
          onPressed: () => Navigator.of(context, rootNavigator: true).pop(),
          color: Colors.grey,
          child: const Text(
            "Oke",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        )
      ],
    ).show();
  }

  void successAlertCallback(
      BuildContext context, String message, Function callback) {
    Alert(
      context: context,
      type: AlertType.success,
      title: "Information",
      desc: message,
      buttons: [
        DialogButton(
          onPressed: () {
            Navigator.pop(context);
            callback();
          },
          color: Colors.grey,
          child: const Text(
            "Oke",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        )
      ],
    ).show();
  }

  void successAlert(BuildContext context, String message) {
    Alert(
      context: context,
      type: AlertType.success,
      title: "Information",
      desc: message,
      buttons: [
        DialogButton(
          onPressed: () {
            Navigator.pop(context);
          },
          color: Colors.grey,
          child: const Text(
            "Oke",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        )
      ],
    ).show();
  }
}
