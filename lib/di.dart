import 'package:derma_travel/data/bloc/detail_tourism/detail_tourism_bloc.dart';
import 'package:derma_travel/data/bloc/login/login_bloc.dart';
import 'package:derma_travel/data/bloc/search_tab/search_tab_bloc.dart';
import 'package:derma_travel/data/repository/login_repository.dart';
import 'package:derma_travel/data/repository/tourism_repository.dart';
import 'package:derma_travel/data/service/api_service.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

void init() {
  locator.registerLazySingleton(() => ApiService.client());

  locator.registerLazySingleton(
    () => LoginRepository(
      client: locator(),
    ),
  );

  locator.registerLazySingleton(
    () => TourismRepository(
      client: locator(),
    ),
  );
  locator.registerFactory(
    () => LoginBloc(loginRepository: locator()),
  );

  locator.registerFactory(
    () => SearchTabBloc(tourismRepository: locator()),
  );

  locator.registerFactory(
    () => DetailTourismBloc(tourismRepository: locator()),
  );
}
