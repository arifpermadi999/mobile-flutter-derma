import 'package:derma_travel/data/service/dio_interceptor.dart';
import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class ApiService {
  static var uri = dotenv.env['BASE_URL'] ?? "";

  static Dio client({int timeout = 30}) {
    BaseOptions options = BaseOptions(
        baseUrl: uri,
        receiveDataWhenStatusError: true,
        responseType: ResponseType.plain,
        connectTimeout: Duration(seconds: timeout), //1 menit
        receiveTimeout: Duration(seconds: timeout), //1 menit
        headers: {"Authorization": "Bearer "},
        validateStatus: (code) {
          if (code == 200) {
            return true;
          }
          return true;
        });
    Dio dio = Dio(options);
    dio.interceptors.add(DioInterceptor());
    dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90));
    return dio;
  }
}
