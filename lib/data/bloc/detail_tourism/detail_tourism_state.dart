part of 'detail_tourism_bloc.dart';

@immutable
sealed class DetailTourismState {}

final class DetailTourismInitial extends DetailTourismState {}

class SetFavoriteState extends DetailTourismState {
  final int isFavorite;
  SetFavoriteState({required this.isFavorite});
}

class LoadingState extends DetailTourismState {}

class ErrorMessageState extends DetailTourismState {
  final String message;
  ErrorMessageState(this.message);
}

class SuccessMessageState extends DetailTourismState {
  final String message;
  SuccessMessageState(this.message);
}
