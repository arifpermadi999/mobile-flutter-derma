part of 'detail_tourism_bloc.dart';

@immutable
sealed class DetailTourismEvent {}

class SetFavoriteEvent extends DetailTourismEvent {
  final int isFavorite;
  final int id;
  SetFavoriteEvent({required this.isFavorite, required this.id});
}
