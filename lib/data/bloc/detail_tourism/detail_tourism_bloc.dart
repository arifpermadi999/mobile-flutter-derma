import 'package:bloc/bloc.dart';
import 'package:derma_travel/data/repository/tourism_repository.dart';
import 'package:meta/meta.dart';

part 'detail_tourism_event.dart';
part 'detail_tourism_state.dart';

class DetailTourismBloc extends Bloc<DetailTourismEvent, DetailTourismState> {
  TourismRepository tourismRepository;

  DetailTourismBloc({required this.tourismRepository})
      : super(DetailTourismInitial()) {
    on<DetailTourismEvent>((event, emit) {
      // TODO: implement event handler
    });
    on<SetFavoriteEvent>((event, emit) async {
      emit(LoadingState());
      await tourismRepository.doSetFavorite(event.isFavorite, event.id);
      emit(SetFavoriteState(isFavorite: event.isFavorite));
    });
  }
}
