part of 'login_bloc.dart';

@immutable
sealed class LoginEvent {}

class LoginActionEvent extends LoginEvent {
  final String email;
  final String password;
  LoginActionEvent(this.email, this.password);
}

class CekSessionLoginEvent extends LoginEvent {
  CekSessionLoginEvent();
}

class VisiblePasswordEvent extends LoginEvent {
  bool isPassword;
  VisiblePasswordEvent(this.isPassword);
}
