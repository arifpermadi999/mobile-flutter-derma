part of 'login_bloc.dart';

@immutable
sealed class LoginState {}

final class LoginInitial extends LoginState {}

class SuccessLoginState extends LoginState {}

class ErrorMessageState extends LoginState {
  final String mesage;
  ErrorMessageState(this.mesage);
}

class LoadingState extends LoginState {}

class HasLoginState extends LoginState {}

class VisiblePasswordState extends LoginState {
  bool isPassword = false;
  VisiblePasswordState(this.isPassword);
}
