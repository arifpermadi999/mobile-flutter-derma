import 'package:bloc/bloc.dart';
import 'package:derma_travel/common/exception.dart';
import 'package:derma_travel/data/pref/auth_pref.dart';
import 'package:derma_travel/data/repository/login_repository.dart';
import 'package:meta/meta.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginRepository loginRepository;
  LoginBloc({required this.loginRepository}) : super(LoginInitial()) {
    on<LoginEvent>((event, emit) {
      // TODO: implement event handler
    });
    on<CekSessionLoginEvent>((event, emit) async {
      var login = await AuthPref().getAuthInfo();
      if (login.accessToken != null) {
        emit(HasLoginState());
      }
    });

    on<VisiblePasswordEvent>((event, emit) async {
      emit(VisiblePasswordState(event.isPassword));
    });
    on<LoginActionEvent>((event, emit) async {
      emit(LoadingState());
      try {
        var response =
            await loginRepository.doLogin(event.email, event.password);
        await AuthPref().save(response);

        emit(SuccessLoginState());
      } on ServerException catch (e) {
        emit(ErrorMessageState(e.message));
      }
    });
  }
}
