part of 'search_tab_bloc.dart';

@immutable
sealed class SearchTabEvent {}

class SearchEvent extends SearchTabEvent {
  final String search;
  SearchEvent(this.search);
}

class CekLoginSessionEvent extends SearchTabEvent {
  CekLoginSessionEvent();
}

class SetFavoriteEvent extends SearchTabEvent {
  final int isFavorite;
  final int index;
  final int id;
  SetFavoriteEvent(
      {required this.index, required this.id, required this.isFavorite});
}

class SearchCloseEvent extends SearchTabEvent {}
