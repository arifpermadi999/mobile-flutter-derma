part of 'search_tab_bloc.dart';

@immutable
sealed class SearchTabState {}

final class MainMenuInitial extends SearchTabState {}

class LoadDataState extends SearchTabState {
  final List<Tourism> data;
  LoadDataState({required this.data});
}

class SetFavoriteState extends SearchTabState {
  final int isFavorite;
  final int index;
  SetFavoriteState({required this.isFavorite, required this.index});
}

class LoadingState extends SearchTabState {}

class ErrorMessageState extends SearchTabState {
  final String message;
  ErrorMessageState(this.message);
}

class SuccessMessageState extends SearchTabState {
  final String message;
  SuccessMessageState(this.message);
}

class SearchCloseState extends SearchTabState {}

class GetSessionLoginState extends SearchTabState {
  AuthModel authModel;
  GetSessionLoginState({required this.authModel});
}
