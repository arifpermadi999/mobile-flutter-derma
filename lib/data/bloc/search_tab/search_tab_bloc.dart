import 'package:bloc/bloc.dart';
import 'package:derma_travel/data/models/auth_model.dart';
import 'package:derma_travel/data/models/tourism_model.dart';
import 'package:derma_travel/data/pref/auth_pref.dart';
import 'package:derma_travel/data/repository/tourism_repository.dart';
import 'package:meta/meta.dart';

part 'search_tab_event.dart';
part 'search_tab_state.dart';

class SearchTabBloc extends Bloc<SearchTabEvent, SearchTabState> {
  TourismRepository tourismRepository;
  SearchTabBloc({required this.tourismRepository}) : super(MainMenuInitial()) {
    on<SearchTabEvent>((event, emit) {});
    on<SearchCloseEvent>((event, emit) async {
      emit(SearchCloseState());
    });
    on<CekLoginSessionEvent>((event, emit) async {
      var authModel = await AuthPref().getAuthInfo();
      emit(GetSessionLoginState(authModel: authModel));
    });
    on<SetFavoriteEvent>((event, emit) async {
      emit(LoadingState());
      await tourismRepository.doSetFavorite(event.isFavorite, event.id);
      emit(SetFavoriteState(isFavorite: event.isFavorite, index: event.index));
    });
    on<SearchEvent>((event, emit) async {
      emit(LoadingState());
      var response = await tourismRepository.doSearch(event.search);

      emit(LoadDataState(data: response));
    });
  }
}
