import 'dart:convert';

import 'package:derma_travel/common/exception.dart';
import 'package:derma_travel/data/models/auth_model.dart';

import 'package:dio/dio.dart';

class LoginRepository {
  Dio client;
  LoginRepository({required this.client});
  Future<AuthModel> doLogin(String email, String password) async {
    final response = await client.post("/login", data: {
      "email": email,
      "password": password,
    });
    if (response.statusCode == 200) {
      return AuthModel.fromJson(json.decode(response.data));
    } else {
      var res = json.decode(response.data);
      throw ServerException(res["error"]);
    }
  }
}
