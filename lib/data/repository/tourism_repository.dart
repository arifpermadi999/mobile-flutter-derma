import 'dart:convert';

import 'package:derma_travel/common/exception.dart';
import 'package:derma_travel/data/models/basic_response.dart';
import 'package:derma_travel/data/models/tourism_model.dart';
import 'package:dio/dio.dart';

class TourismRepository {
  Dio client;
  TourismRepository({required this.client});
  Future<List<Tourism>> doSearch(String search) async {
    final response = await client.get("/tourism?search=$search");
    if (response.statusCode == 200) {
      return parseTourismList(response.data);
    } else {
      var res = json.decode(response.data);
      throw ServerException(res["error"]);
    }
  }

  Future<BasicResponse> doSetFavorite(int favorite, int id) async {
    final response = await client.post("/tourism/setFavorite",
        data: {"id": id.toString(), "favorite": favorite});
    if (response.statusCode == 200) {
      return BasicResponse.fromJson(json.decode(response.data));
    } else {
      var res = json.decode(response.data);
      throw ServerException(res["error"]);
    }
  }
}
