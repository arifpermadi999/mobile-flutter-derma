import 'dart:convert';

import 'package:derma_travel/data/models/auth_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthPref {
  static String key = "auth";
  read() async {
    final prefs = await SharedPreferences.getInstance();
    String prefData = prefs.getString(key).toString();
    return json.decode(prefData);
  }

  save(value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, json.encode(value));
  }

  saveLoginTemp() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, "login");
  }

  logoutTemp() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
    await prefs.clear();
  }

  Future<bool> isLogin() async {
    final prefs = await SharedPreferences.getInstance();
    String login = prefs.getString(key).toString();

    return login == "null" ? false : true;
  }

  saveRememberMe(username, password) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString("username_remember", username);
    prefs.setString("password_remember", password);
  }

  clearRememberMe() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString("username_remember", "");
    prefs.setString("password_remember", "");
  }

  logout() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
    await prefs.clear();
    //userRememberMe = prefs.getString("username_remember") ?? "";
    //passRememberMe = prefs.getString("password_remember") ?? "";
    // final prefs2 = await SharedPreferences.getInstance();
    // //prefs2.setString("username_remember", userRememberMe);
    // //prefs2.setString("password_remember", passRememberMe);
  }

  remove(String key) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }

  // static getRememberMe() async {
  //   final prefs = await SharedPreferences.getInstance();
  //   userRememberMe = prefs.getString("username_remember") ?? "";
  //   passRememberMe = prefs.getString("password_remember") ?? "";
  // }

  Future<AuthModel> getAuthInfo() async {
    try {
      AuthPref authPref = AuthPref();
      var authPrefData = await authPref.read();
      if (authPrefData == null) {
        return AuthModel();
      }
      AuthModel authModel = AuthModel.fromJson(authPrefData);
      return authModel;
    } catch (exception) {
      //print(exception.toString());
      return AuthModel();
    }
  }
}
