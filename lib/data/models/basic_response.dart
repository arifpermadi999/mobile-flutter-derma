class BasicResponse {
  String? message;

  BasicResponse({this.message});

  BasicResponse.fromJson(dynamic json) {
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["message"] = message;
    return map;
  }
}
