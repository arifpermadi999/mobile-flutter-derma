// tourism_model.dart

import 'dart:convert';

class Tourism {
  int? id;
  String? name;
  String? image;
  int? isFavorite;
  String? latlong;
  String? invitationText;
  String? description;
  DateTime? createdAt;
  DateTime? updatedAt;

  Tourism({
    this.id,
    this.name,
    this.image,
    this.isFavorite,
    this.latlong,
    this.invitationText,
    this.description,
    this.createdAt,
    this.updatedAt,
  });

  factory Tourism.fromJson(Map<String, dynamic> json) {
    return Tourism(
      id: json['id'],
      name: json['name'],
      image: json['image'],
      isFavorite: json['is_favorite'],
      latlong: json['latlong'],
      invitationText: json['invitation_text'] ?? "",
      description: json['description'] ?? "",
      createdAt: DateTime.parse(json['createdAt']),
      updatedAt: DateTime.parse(json['updatedAt']),
    );
  }
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'image': image,
      'is_favorite': isFavorite,
      'latlong': latlong,
      'invitation_text': invitationText,
      'description': description,
      'createdAt': createdAt?.toIso8601String(),
      'updatedAt': updatedAt?.toIso8601String(),
    };
  }
}

List<Tourism> parseTourismList(String jsonStr) {
  var parsed = jsonDecode(jsonStr).cast<Map<String, dynamic>>();
  return parsed.map<Tourism>((json) => Tourism.fromJson(json)).toList();
}
