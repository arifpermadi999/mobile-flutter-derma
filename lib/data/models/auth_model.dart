import 'package:derma_travel/data/models/user_model.dart';

class AuthModel {
  String? accessToken;
  User? user;

  AuthModel({this.accessToken, this.user});

  AuthModel.fromJson(Map<String, dynamic> json) {
    accessToken = json['token'];
    user = json['user'] != null ? User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['token'] = accessToken;
    if (user != null) {
      data['user'] = user!.toJson();
    }
    return data;
  }
}
