import 'package:derma_travel/data/bloc/detail_tourism/detail_tourism_bloc.dart';
import 'package:derma_travel/data/bloc/login/login_bloc.dart';
import 'package:derma_travel/data/bloc/search_tab/search_tab_bloc.dart';
import 'package:derma_travel/data/models/tourism_model.dart';
import 'package:derma_travel/presentation/login_screen.dart';
import 'package:derma_travel/presentation/main_menu.dart';
import 'package:derma_travel/presentation/tab/detail_tourism.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:derma_travel/di.dart' as di;
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

Future<void> main() async {
  di.init();
  await dotenv.load(fileName: ".env");
  runApp(const MyApp());
}

final router = GoRouter(
  initialLocation: '/',
  routes: [
    GoRoute(
        path: '/',
        builder: (context, state) {
          return BlocProvider(
            create: (context) => di.locator<LoginBloc>(),
            child: const LoginScreen(),
          );
        },
        routes: [
          GoRoute(
            name: MainMenu.route,
            path: MainMenu.route,
            builder: (context, state) => BlocProvider(
              create: (context) => di.locator<SearchTabBloc>(),
              child: const MainMenu(),
            ),
          ),
          GoRoute(
            path: DetailTourism.route,
            builder: (context, state) {
              Tourism tourism = Tourism();

              if (state.extra != null) {
                tourism = state.extra as Tourism;
              }
              return BlocProvider(
                create: (context) => di.locator<DetailTourismBloc>(),
                child: DetailTourism(
                  tourism: tourism,
                ),
              );
            },
          ),
        ]),
  ],
);

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      routerConfig: router,
    );
  }
}
